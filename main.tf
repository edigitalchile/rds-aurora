locals {
  db_creds = jsondecode(data.aws_secretsmanager_secret_version.creds.secret_string)
}



data "aws_secretsmanager_secret_version" "creds" {
  # Fill in the name you gave to your secret
  secret_id = var.kms_secret_id
}

module "aurora" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "~> 2.0"
  count = var.engine_mode == "serverless" ? 0 : 1

  name                            = var.name 
  engine                          = var.engine
  engine_version                  = var.engine_version
  engine_mode                     = var.engine_mode
  subnets                         = var.subnets
  vpc_id                          = var.vpc_id
  replica_count                   = var.quantity_instance
  instance_type                   = var.instance_type
  apply_immediately               = var.apply_immediately
  skip_final_snapshot             = var.skip_final_snapshot
  db_parameter_group_name         = aws_db_parameter_group.instances.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.cluster.id
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports
  create_security_group           = var.create_default_security_group
  vpc_security_group_ids          = var.vpc_security_group_ids
  allowed_cidr_blocks             = var.allowed_cidr_blocks
  backup_retention_period         = var.backup_retention_period
  preferred_maintenance_window    = var.preferred_maintenance_window
  preferred_backup_window         = var.preferred_backup_window
  deletion_protection             = var.deletion_protection
  username = local.db_creds.username
  password = local.db_creds.password
  tags                    = var.tags


}


module "aurora_serverless" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "~> 2.0"
  count = var.engine_mode == "serverless" ? 1 : 0


  name                            = var.name 
  engine                          = var.engine
  engine_version                  = var.engine_version
  engine_mode                     = var.engine_mode
  subnets                         = var.subnets
  vpc_id                          = var.vpc_id
  replica_count                   = 0
  instance_type                   = var.instance_type
  apply_immediately               = var.apply_immediately
  skip_final_snapshot             = var.skip_final_snapshot
  db_parameter_group_name         = aws_db_parameter_group.instances.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.cluster.id
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports
  create_security_group           = var.create_default_security_group
  vpc_security_group_ids          = var.vpc_security_group_ids
  allowed_cidr_blocks             = var.allowed_cidr_blocks
  backup_retention_period         = var.backup_retention_period
  preferred_maintenance_window    = var.preferred_maintenance_window
  preferred_backup_window         = var.preferred_backup_window
  deletion_protection             = var.deletion_protection
  username = local.db_creds.username
  password = local.db_creds.password

  scaling_configuration  = {
    auto_pause               = var.serverless_auto_pause
    min_capacity             = var.serverless_min_capacity
    max_capacity             = var.serverless_max_capacity

  }
  tags                    = var.tags


}


resource "aws_db_parameter_group" "instances" {
  name        = var.parameter_group_instance_name
  family      = var.parameter_group_instance_family
  description = var.parameter_group_instance_description

  dynamic "parameter" {
    for_each = var.parameter_group_instance_config

    content {
      name  = parameter.value.name
      value = parameter.value.value
    }
  }
  tags                    = var.tags

  
}

resource "aws_rds_cluster_parameter_group" "cluster" {
  name        = var.parameter_group_cluster_name
  family      = var.parameter_group_cluster_family
  description = var.parameter_group_cluster_description

  dynamic "parameter" {
    for_each = var.parameter_group_cluster_config

    content {
      name  = parameter.value.name
      value = parameter.value.value
    }
  }
  tags                    = var.tags

}