# REQUIRED VARIABLES

variable name {
  type        = string
  description = "Nombre del cluster de la base de datos"
}

variable engine {
  type        = string
  description = "Motor de la base de datos aurora"
}

variable engine_version {
  type        = string
  description = "Version del motor de la base de datos"
}

variable subnets {
  type        = list(string)
  description = "Lista de subnets donde podran alojarse las instancias"
}

variable vpc_id {
  type        = string
  description = "Id de la vpc donde se ubicara el cluster"
}

variable instance_type {
    type = string
    description = "Tipo de instancia de la base de datos"
}

variable vpc_security_group_ids {
    type = list(string)
    description = "Lista de security groups a aplicar"
}


variable parameter_group_instance_name {
    type = string
    description = "Nombre del parameter group para las instancias"

}

variable parameter_group_instance_family {
    type = string
    description = "Familia del parameter group para las instancias"

}

variable parameter_group_cluster_name {
    type = string
    description = "Nombre del parameter group para el cluster"

}

variable parameter_group_cluster_family {
    type = string
    description = "Familia del parameter group para el cluster"

}

variable allowed_cidr_blocks {
    type = list(string)
    description = "Lista de redes que pueden acceder a la bd"
}

variable kms_secret_id {
    type = string
    description = "Nombre de la clave en kms"
}


#OPTIONAL VARIABLES

variable quantity_instance {
    type  = number 
    description = "Cantidad de instancias de base de datos a crear"
    default = 2
}

variable parameter_group_instance_description {
    type = string
    description = "Descripcion del parameter group para las instancias"
    default = "Description for parameter group instances created by terraform"

}

variable parameter_group_cluster_description {
    type = string
    description = "Descripcion del parameter group para el cluster"
    default = "Description for parameter group cluster created by terraform"

}

variable create_default_security_group {
    type = bool
    description = "Crear security group por defecto"
    default = false 
}

variable enabled_cloudwatch_logs_exports {
    type = list(string)
    description = "Logs que se enviaran a cloudwatch"
    default = []
}

variable skip_final_snapshot {
    type = bool
    description = "No crear snapshot en la destruccion del cluster"
    default = true
}

variable apply_immediately {
    type = bool
    description = "Aplicar cambios ahora o en la proxima ventana de mantenimiento"
    default = false
}

variable preferred_maintenance_window {
    type = string
    description = "Tiempo para la ventana de mantenimiento"
    default = "sun:05:00-sun:06:00"
}

variable preferred_backup_window {
    type = string
    description = "Tiempo para la ventana de backup"
    default = "02:00-03:00"
}

variable parameter_group_instance_config {
  type        = list(map(string))
  description = "Configuracion de los parameter groups para instancias"
  default     = []
}


variable parameter_group_cluster_config {
  type        = list(map(string))
  description = "Configuracion de los parameter groups para cluster"
  default     = []
}

variable engine_mode {
    type = string
    description = "Modo del motor de la base de datos"
    default = "provisioned"
}

variable backup_retention_period {
    type = number
    description = "Numero de dias para el almacenaminto de la copia de seguridad"
    default = 7
}

variable serverless_auto_pause {
    type = bool
    description = "Indica si detiene la instancia si no hay peticiones"
    default = false

}

variable serverless_min_capacity {
    type = number
    description = "Numero minimo de instancias"
    default = 2
    
}


variable serverless_max_capacity {
    type = number
    description = "Numero maximo de instancias"
    default = 8
    
}

variable deletion_protection {
    type = bool
    description = "Prohibir borrado de la BD a traves de la api"
    default = false
}

variable tags {
  type        = map(string)
  description = "Mapa de tags para asignar a los recursos de BBDD"
  default     = null
}

