output "common_rds" {
  value = module.aurora.*
}

output "serverless_rds" {
  value = module.aurora_serverless.*
}