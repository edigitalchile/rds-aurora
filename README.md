# Modulo de terraform para crear base de datos en rds aurora

Este modulo esta basado en el modulo oficial de terraform para la creacion de base datos auroras en rds: https://registry.terraform.io/modules/terraform-aws-modules/rds-aurora/aws/2.29.0  


### Inputs

| Nombre  | Descripción  | Requerido | Default  |
|---|---|---|---|
| name  | Nombre del cluster de la base de datos  | SI  | N/A  |
| engine  | Motor de la base de datos aurora  | SI  | N/A  |
| engine_version  | Version del motor de la base de datos  | SI  | N/A  |
| subnets  | Lista de subnets donde podran alojarse las instancias  | SI  | N/A  |
| vpc_id  | Id de la vpc donde se ubicara el cluster  | SI  | N/A  |
| instance_type  | Tipo de instancia de la base de datos  | SI  | N/A  |
| vpc_security_group_ids  | Lista de security groups a aplicar  | SI  | N/A  |
| parameter_group_instance_name  | Nombre del parameter group para las instancias  | SI  | N/A  |
| parameter_group_instance_family  | Familia del parameter group para las instancias  | SI  | N/A  |
| parameter_group_cluster_name  | Nombre del parameter group para el cluster  | SI  | N/A  |
| parameter_group_cluster_family  | Familia del parameter group para el cluster  | SI  | N/A  |
| allowed_cidr_blocks  | Lista de redes que pueden acceder a la bd  | SI  | N/A  |
| kms_secret_id  | Nombre de la clave en kms  | SI  | N/A  |
| quantity_instance  | Cantidad de instancias de base de datos a crear  | NO  | 2  |
| parameter_group_instance_description  | Descripcion del parameter group para las instancias  | NO  | Description for parameter group instances created by terraform  |
| parameter_group_cluster_description  | Descripcion del parameter group para el cluster  | NO  | Description for parameter group cluster created by terraform  |
| create_default_security_group  | Crear security group por defecto  | NO  | false  |
| enabled_cloudwatch_logs_exports  | Logs que se enviaran a cloudwatch  | NO  | []  |
| skip_final_snapshot  | No crear snapshot en la destruccion del cluster  | NO  | true  |
| apply_immediately  | Aplicar cambios ahora o en la proxima ventana de mantenimiento  | NO  | false  |
| preferred_maintenance_window  | Tiempo para la ventana de mantenimiento  | NO  | sun:05:00-sun:06:00  |
| preferred_backup_window  | Tiempo para la ventana de backup  | NO  | 02:00-03:00  |
| parameter_group_instance_config  | Configuracion de los parameter groups para instancias  | NO  | []  |
| parameter_group_cluster_config  | Configuracion de los parameter groups para cluster  | NO  | []  |
| engine_mode  | Modo del motor de la base de datos  | NO  | provisioned  |
| backup_retention_period  | Numero de dias para el almacenaminto de la copia de seguridad  | NO  | 7  |
| serverless_auto_pause  | Indica si detiene la instancia si no hay peticiones  | NO  | false  |
| serverless_min_capacity  | Numero minimo de instancias  | NO  | 2  |
| serverless_max_capacity  | Numero maximo de instancias  | NO  | 8  |
| deletion_protection  | Prohibir borrado de la BD a traves de la api  | NO  | false  |
| tags  | Mapa de tags para asignar a los recursos de BBDD  | NO  | null  |



### Outputs

| Nombre  | Descripción  |
|---|---|
| common_rds  | Array de una posicion que contiene el objeto con la informacion de las base de datos aurora creada, si y solo si no es serverless. Los atributos son lo que aparecen en la documentacion del modulo oficial de rds-aurora  |
| serverless_rds  | Array de una posicion que contiene el objeto con la informacion de las base de datos aurora creada, si y solo si es serverless. Los atributos son lo que aparecen en la documentacion del modulo oficial de rds-aurora  |